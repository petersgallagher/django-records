from __future__ import unicode_literals

from django.db import models


class Artist(models.Model):
    name = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Format(models.Model):
    VINYL = 'VNL'
    CASSETTE = 'CAS'
    CD = 'CD'
    MP3 = 'MP3'
    MEDIUMS = (
        (VINYL, 'Vinyl'),
        (CASSETTE, 'Cassette'),
        (CD, 'CD'),
        (MP3, 'MP3')
    )
    name = models.TextField()
    medium = models.CharField(max_length=3, choices=MEDIUMS)

    def __str__(self):
        return self.name


class Track(models.Model):
    artists = models.ManyToManyField(Artist)
    title = models.TextField()
    duration = models.DurationField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Tracklist(models.Model):
    name = models.TextField()
    tracks = models.ManyToManyField(Track, through='TrackPosition')

    def __str__(self):
        return self.name


class TrackPosition(models.Model):
    track = models.ForeignKey(Track)
    tracklist = models.ForeignKey(Tracklist)
    disc = models.CharField(max_length=1)
    number = models.IntegerField()


class Record(models.Model):
    artists = models.ManyToManyField(Artist)
    title = models.TextField()
    tracklist = models.OneToOneField(Tracklist)
    format = models.ForeignKey(Format)
    released = models.DateField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


