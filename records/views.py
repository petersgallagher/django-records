from rest_framework import viewsets
from records.serializers import RecordSerializer, ArtistSerializer, FormatSerializer, TrackSerializer, TracklistSerializer, TrackPositionSerializer
from records.models import Record, Artist, Format, Track, Tracklist, TrackPosition


class ArtistViewSet(viewsets.ModelViewSet):
    queryset = Artist.objects.all().order_by('name')
    serializer_class = ArtistSerializer


class FormatViewSet(viewsets.ModelViewSet):
    queryset = Format.objects.all().order_by('name')
    serializer_class = FormatSerializer


class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.all().order_by('title')
    serializer_class = TrackSerializer


class TracklistViewSet(viewsets.ModelViewSet):
    queryset = Tracklist.objects.all()
    serializer_class = TracklistSerializer


class TrackPositionViewSet(viewsets.ModelViewSet):
    queryset = TrackPosition.objects.all()
    serializer_class = TrackPositionSerializer


class RecordViewSet(viewsets.ModelViewSet):
    queryset = Record.objects.all().order_by('-created')
    serializer_class = RecordSerializer
