from rest_framework import serializers
from records.models import Record, Artist, Format, Track, Tracklist, TrackPosition


class ArtistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'


class FormatSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Format
        fields = '__all__'


class TrackSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Track
        fields = '__all__'


class TracklistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tracklist
        fields = '__all__'


class TrackPositionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TrackPosition
        fields = '__all__'


class RecordSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Record
        fields = '__all__'
