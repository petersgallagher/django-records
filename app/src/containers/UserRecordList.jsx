import { connect } from 'react-redux'
import RecordList from '../components/RecordList'

function getUserRecords(records, user) {
  return records;
}

const mapStateToProps = (state) => {
  return {
    records: getUserRecords(state.records, state.user)
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
  }
};

const UserRecordList = connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordList);

export default UserRecordList;