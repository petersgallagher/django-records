import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import catalogueApp from './reducers'
import Catalogue from './components/Catalogue'

let store = createStore(catalogueApp);

ReactDOM.render(
  <Provider store={store}>
    <Catalogue />
  </Provider>,
  document.getElementById('app')
);