import React from 'react'
import UserRecordList from '../containers/UserRecordList'

const Catalogue = () => {
  return (
    <div>
      <UserRecordList />
    </div>
  )
};

export default Catalogue;