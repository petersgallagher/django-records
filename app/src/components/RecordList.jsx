import React, { PropTypes } from 'react'
import Record from './Record'

function RecordList(props) {
  return (
    <ul>
      {props.records.map( record =>
        <Record key={record.id} title={record.title} />
      )}
    </ul>
  )
}

RecordList.propTypes = {
  records: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string
  })).isRequired,
};

export default RecordList;