import React, { PropTypes } from 'react'

function Record(props) {
  return (
    <li>{props.title}</li>
  );
}

Record.propTypes = {
  title: PropTypes.string
};

export default Record;