const record = (state = {}, action) => {
  return {id: 1, title: 'Test record'};
};

const records =  (state = [], action) => {
  return [{
    id: 1,
    title: 'Test record in list',
  }, {
    id: 2,
    title: 'Another record',
  }]
};

export default records;