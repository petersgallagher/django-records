import { combineReducers } from 'redux'
import records from './records'

const catalogueApp = combineReducers({
  records,
});

export default catalogueApp